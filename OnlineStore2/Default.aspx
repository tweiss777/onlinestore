﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="OnlineStore2.Default" %>
<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <title>PowerTools Inc. Home</title>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="body" runat="server">
        <div class="container-fluid">
            <div class="jumbotron">
                <h1>PowerTools Inc.</h1>
            </div>
            <p>Feel free to take a look at our latest offerings below!! </p>
            <asp:GridView ID="gvTopProducts" AutoGenerateColumns="false" Width="100%" DataKeyNames="ProductID" PageSize="5" runat="server">
                <Columns>
                    <asp:BoundField DataField="ProductName" />
                    <asp:BoundField DataField="ProductDescription" />
                    <asp:BoundField DataField="ProductPrice" />
                    <asp:ImageField DataImageUrlField="ProductID" DataImageUrlFormatString="../imgHandler.ashx?imgID={0}" ControlStyle-Height="50" ControlStyle-Width="50"></asp:ImageField>    
                </Columns>
            </asp:GridView>
        </div>
</asp:Content>
