﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OnlineStore2.DBClasses;
using System.IO;

namespace OnlineStore2
{
    /// <summary>
    /// The purpose of this generic handler will:
    /// 1) convert the image to binary data.
    /// 2) provide a url our image tags can use to display data from the database.
    /// </summary>
    public class imgHandler : IHttpHandler 
    {

        public void ProcessRequest(HttpContext context)
        {
            Byte[] img = null;//create an empty byte array.
            ProductTier pt = new ProductTier();//initialize a new product tier object.

            Int32 imgID = Int32.Parse(context.Request.QueryString["imgID"]);//convert image id to 32 bit since only that is supported by the database.
            img = pt.getProductImage(imgID); //calling this gets the image in the form of a byte array.

            if (img != null)//Check if there is an image or not
            { //Case in which there is an image
                //this is how the array will be written to the image tag.
                context.Response.BinaryWrite(img);
            }
            else
            {//Case in which there isn't
                //set a path to a default image.
                var path = HttpContext.Current.Server.MapPath("/Images/ImageNotFound.png");
                
                //create a uri object that will hold the path of the image.
                Uri uri = new Uri(path);

                //Read the bytes of the uri 
                img = File.ReadAllBytes(uri.LocalPath);

                //Finally write image to image tag
                context.Response.BinaryWrite(img);
                
            }

           

            
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}