﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CustomerRegistration.aspx.cs" Inherits="OnlineStore2.CustomerRegistration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function btnReset() {
            document.getElementById('<%=txtFirstName.ClientID%>').value = "";
            document.getElementById('<%=txtLastName.ClientID%>').value = "";
            document.getElementById('<%=txtEmail.ClientID%>').value = "";
            document.getElementById('<%=txtAddress.ClientID%>').value = "";
            document.getElementById('<%=txtApt.ClientID%>').value = "";
            document.getElementById('<%=txtCity.ClientID%>').value = "";
            document.getElementById('<%=txtState.ClientID%>').value = "";
            document.getElementById('<%=txtZip.ClientID%>').value = "";

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat='server'>
    <div class="container">
        <form id="form1" runat="server">
            <div class="panel panel-success">
                <div class="panel-heading text-center">
                    <h3>User Registration</h3>
                </div>
                <div class="panel-body">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="BulletList" ShowSummary="true" ForeColor="Red" HeaderText="Please fix the following" />
                    <!--Form controls are below--><!--Fix form into a table-->
                    <table>
                        <!--First Name-->
                        <tr>
                            <td>
                                 <label>
                                    First Name *
                                </label>
                                <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" placeHolder="First Name" ></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvFirst" runat="server" ErrorMessage="Name is missing" ForeColor="Red" ControlToValidate="txtFirstName"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <!--Regular expression validator for first name -->
                                <asp:RegularExpressionValidator ID="regexFirst" runat="server" ErrorMessage="First Name must be 3 to 10 letters long. Only letters may be used" ValidationExpression="[a-zA-Z_ ]{3,10}" ControlToValidate="txtFirstName" ForeColor="Red"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <!--Last Name-->
                        <tr>
                            <td>
                                 <label>
                                    Last Name *
                                </label>
                                <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" placeHolder="Last Name"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <!--Regular expression validator for last name-->
                                 <asp:RequiredFieldValidator ID="rfvLast" runat="server" ErrorMessage="Name is missing" ForeColor="Red" ControlToValidate="txtLastName"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RegularExpressionValidator ID="regexLast" runat="server" ErrorMessage="Last Name must be 3 to 10 letters long. Only letters may be used" ValidationExpression="[a-zA-Z_ ]{3,10}" ControlToValidate="txtLastName" ForeColor="Red"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <!--Email-->
                        <tr>
                            <td>
                                <label>
                                    Email * 
                                </label>
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" placeHolder="Email"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="Email is missing" ForeColor="Red" ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RegularExpressionValidator ID="regexEmail" runat="server" ErrorMessage="Invalid Email" ValidationExpression="^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$" ControlToValidate="txtEmail" ForeColor="Red"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                         <!--addr1-->
                        <tr>
                            <td>
                                <label>
                                    Address *
                                </label>
                                <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" placeHolder="Address"></asp:TextBox>
                            </td> 
                        </tr>
                        <tr>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvAddr" runat="server" ErrorMessage="Address is Missing" ForeColor="Red" ControlToValidate="txtAddress"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    Address 2
                                </label>
                                <asp:textbox ID="txtAddress2" CssClass="form-control" placeHolder="Secondary Adress" runat="server"></asp:textbox>
                            </td>
                        </tr>
                        <tr>
                            <!--apt-->
                            <td>
                                <label>Apt #</label>
                                <asp:TextBox ID="txtApt" runat="server" CssClass="form-control" placeHolder="Apt #"></asp:TextBox>
                            </td>
                        </tr>
                        <!--city-->
                        <tr>
                            <td>           
                                <label>
                                    City *
                                </label>
                                <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" placeHolder="City"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvCity" runat="server" ErrorMessage="City is missing" ForeColor="Red" ControlToValidate="txtCity"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RegularExpressionValidator ID="regexCity" runat="server" ErrorMessage="Invalid City" ValidationExpression="[a-zA-Z_ ]{2,20}" ForeColor="Red" ControlToValidate="txtCity"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <!--state-->
                        <tr>
                            <td>
                                 <label>State/Country *</label>
                                 <asp:TextBox ID="txtState" runat="server" CssClass="form-control" placeHolder="First Name"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvState" runat="server" ErrorMessage="State/Country missing" ForeColor="Red" ControlToValidate="txtState"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <asp:RegularExpressionValidator ID="regexState" runat="server" ErrorMessage="Invalid State/Country" ValidationExpression="[a-zA-Z_ ]{2,20}" ForeColor="Red" ControlToValidate="txtState"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <!--zip-->
                        <tr>
                            <td>
                                <label>Zip</label>
                                <asp:TextBox ID="txtZip" runat="server" CssClass="form-control" placeHolder="First Name"></asp:TextBox><br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RegularExpressionValidator ID="regexZip" runat="server" ErrorMessage="Invalid zip" ValidationExpression="^\d{5}$" ControlToValidate="txtZip"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                    
                    <div>
                        <asp:Button CssClass="btn btn-default" ID="btnSubmit" runat="server" Text="Submit" OnClick="btn_submit" />
                        <asp:Button CssClass="btn btn-default" ID="btnReset" runat="server" Text="Reset"  CausesValidation="false" OnClientClick="btnReset()"/>
                    </div>

                </div>
            </div>

        </form>
    </div>
</asp:Content>
