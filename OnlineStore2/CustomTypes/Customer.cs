﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineStore2.CustomTypes
{
    public class Customer
    {
        //firstname, lastname, email, address, apt, city, state, zip
        //state could also be a country
        private String firstName;
        private String lastName;
        private String email;
        private String address;
        private String address2;
        private String apt;
        private String city;
        private String state;
        private int zip;

        public Customer() //one way of setting constructors using computed properties.
            //However, can be done without computed properties similar to Java
        {
            Fname = "";
            Lname = "";
            Email = "";
            Address = "";
            Address2 = "";
            City = "";
            State = "";
            Zip = 00000;

        }

        public String Fname
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }
        }

        public String Lname
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }

        public String Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }

        public String Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }
        public String Address2
        {
            get
            {
                return address2;
            }
            set
            {
                address2 = value;
            }
        }

        public String Apt
        {
            get
            {
                return apt;
            }
            set
            {
                apt = value;
            }
        }

        public String City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }

        public String State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
            }
        }

        public int Zip
        {
            get
            {
                return zip;
            }
            set
            {
                zip = value;   
            }
        }
    }
}