﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineStore2.CustomTypes
{ //this is a class similar to the customer class
    //unknown if this class would be needed.

    public class Product
    {
        //global fields
        private String productDescription;
        private String productName;
        private float productPrice;
        private int quantityOnHand;
        private int departmentID;
        private int categoryID;

        //constructor
        public Product()
        {
            description = "";
            name = "";
            price = 0;
            quantity = 0;
            deptID = 0;
            catID = 0;

          
        }

        //computed property
        public String description
        {
            get
            {
                return productDescription;
            }

            set
            {
                productDescription = value;
            }

        }

        public String name
        {
            get
            {
                return productName;
            }

            set
            {
                productName = value;
            }
        }

        public float price
        {
            get
            {
                return productPrice;
            }

            set
            {
                productPrice = value;
            }
        }

        public int quantity
        {
            get
            {
                return quantityOnHand;
            }
            set
            {
                quantityOnHand = value;
            }
        }

        public int deptID
        {
            get
            {
                return departmentID;
            }
            set
            {
                departmentID = value;
            }
        }

        public int catID
        {
            get
            {
                return categoryID;
            }
            set
            {
                categoryID = value;
            }
        }
    }

}