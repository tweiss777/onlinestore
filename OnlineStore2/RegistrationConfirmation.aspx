﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RegistrationConfirmation.aspx.cs" Inherits="OnlineStore2.RegistrationConfirmation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading text-center">
                <h3>Confirmation</h3>
            </div>
            <div class="panel-body" style="font-size: large">
                <div class="container ">
                    <div class="row"> <!--row for firstname -->
                        <div class="col-md-5 col-sm-5">
                            <h4>First Name:</h4>
                        </div>
                        <div class="col-md-5 col-sm-5 ">
                            <h4 id="fName" runat="server"> </h4>
                        </div>
                    </div>
                    <div class="row"> <!-- row for last name-->
                        <div class="col-md-5 col-sm-5">
                            <h4>Last Name:</h4>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <h4 id="lName" runat="server"></h4>
                        </div>
                    </div>
                    <div class="row"> <!-- row for email address -->
                        <div class="col-md-5 col-sm-5">
                            <h4>Email:</h4>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <h4 id="email" runat="server"></h4>
                        </div>
                    </div>
                    <div class="row"> <!--row for adress 1-->
                        <div class="col-md-5 col-sm-5">
                            <h4>Address:</h4>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <h4 id="addr" runat="server"></h4>
                        </div>
                    </div>
                    <div class="row"><!-- row for address 2-->
                        <div class="col-md-5 col-sm-5">
                            <h4>Address 2:</h4>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <h4 id="addr2" runat="server"></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-sm-5">
                            <h4>Apt #:</h4>
                        </div><div class="col-md-5 col-sm-5">
                            <h4 id="apt" runat="server"></h4>
                        </div>
                        
                    </div>
                    <div class="row"> <!-- row for city name -->
                        <div class="col-md-5 col-sm-5">
                            <h4>City</h4>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <h4 id="city" runat="server"></h4>
                        </div>
                    </div>
                    <div class="row"> <!-- row for state -->
                        <div class="col-md-5 col-sm-5">
                            <h4>State:</h4>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <h4 id="state" runat="server"></h4>
                        </div>
                    </div>
                    <div class="row"> <!-- row for zip -->
                        <div class="col-md-5 col-sm-5">
                            <h4>Zip:</h4>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <h4 id="zip" runat="server"></h4>
                        </div>
                    </div>
                </div>
                
                <form id="form1" runat="server">
                    <asp:Button ID="btnsubmit" runat="server" Text="Submit" OnClick="btnSubmit" /><!-- the button to submit to the database -->
                    <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClick="btnCancel" />
                </form>
                
            </div>
        </div>
    </div>
</asp:Content>
