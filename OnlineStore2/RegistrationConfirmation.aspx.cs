﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnlineStore2.CustomTypes;

//importing the datatier library below for storing customer in the database.
using OnlineStore2.DBClasses;  

namespace OnlineStore2
{
    public partial class RegistrationConfirmation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["CustomerRegistration"] != null)
            { //loads the information from the session variable if the session variable is not null
                Customer customer = (Customer)Session["CustomerRegistration"];
                fName.InnerHtml = customer.Fname;
                lName.InnerHtml = customer.Lname;
                email.InnerHtml = customer.Email;
                addr.InnerHtml = customer.Address;
                addr2.InnerHtml = customer.Address2;
                apt.InnerHtml = customer.Apt;
                city.InnerHtml = customer.City;
                state.InnerHtml = customer.State;
                zip.InnerHtml = customer.Zip.ToString();
                Console.Write(customer.Fname);
                
            }
            else
            {
                Response.Redirect("CustomerRegistration.aspx");
            }
        }

        protected void btnSubmit(object sender, EventArgs e)
        {
            CustomerTier ct = new CustomerTier();
            bool was_success = ct.insertCustomer(fName.InnerText, lName.InnerText, addr.InnerText + " " + apt.InnerText, addr2.InnerText, city.InnerText, state.InnerText, int.Parse(zip.InnerText));
            if(was_success == true)
            {
                Response.Redirect("ThankYou.aspx");
            }
            else
            {
                Response.Redirect("404Redirect.aspx");
            }
            
        }

        protected void btnCancel(object sender, EventArgs e)
        {
            Response.Redirect("CustomerRegistration.aspx");
        }
    }
}