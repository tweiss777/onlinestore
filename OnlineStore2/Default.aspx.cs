﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnlineStore2.DBClasses;
using System.IO;

namespace OnlineStore2
{
    public partial class Default : System.Web.UI.Page
    {
        private Byte[] convert_image(Stream fstream)
        {
            //Pass this as arg to fstream  when calling: FileUpload1.PostedFile.InputStream;

            BinaryReader br = new BinaryReader(fstream);//new binary reader holding the fstream.


            Byte[] img = br.ReadBytes((Int32)fstream.Length); //write the image to a byte array. typecast to 32 bit in
            return img;//return the byte array to be passed

        }

        /*This method basically shuts up the error that your 
         GridView Tags need runat="server"*/
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}