﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnlineStore2.CustomTypes;

namespace OnlineStore2
{
    public partial class CustomerRegistration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {//this is what executes when the page loads
            if (Session["CustomerRegistration"] != null)
            {
                Customer customer = (Customer)Session["CustomerRegistration"];
                txtFirstName.Text = customer.Fname ;
                txtLastName.Text = customer.Lname;
                txtEmail.Text = customer.Email;
                txtAddress.Text = customer.Address;
                txtAddress2.Text = customer.Address2;
                txtApt.Text = customer.Apt;
                txtCity.Text = customer.City;
                txtState.Text = customer.State;
                txtZip.Text = customer.Zip.ToString();
                Session["CustomerRegistration"] = null;
            }
        }



        protected void btn_submit(object sender, EventArgs e)
        { //this is the event handler for when information is submitted
            int intZip;
            Customer customer = new CustomTypes.Customer(); //create a new customer object.
            customer.Fname = txtFirstName.Text;
            customer.Lname = txtLastName.Text;
            customer.Email = txtEmail.Text;
            customer.Address = txtAddress.Text;
            customer.Apt = txtApt.Text;
            customer.City = txtCity.Text;
            customer.State = txtState.Text;
            Console.Write(customer.Fname);

            if (txtAddress2.Text.Length == 0)
            {
                customer.Address2 = ""; //just add an empty string if there is nothing in the textbox
            }
            else
            {
                customer.Address2 = txtAddress2.Text; //Just add what is inside the text box
            }

            try
            {
                Console.Write("attempting to typecast zipInt");
                intZip = Int32.Parse(txtZip.Text);
                customer.Zip = intZip;
            }
            catch(FormatException ex)
            {
                Console.WriteLine("typecast failed");
                Console.Write(ex.Message);
            }
            Session["CustomerRegistration"] = customer;
            Response.Redirect("RegistrationConfirmation.aspx");
        }
    }


}