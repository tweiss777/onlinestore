﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="OnlineStore2.Contact" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="container">
        <div class="jumbotron">
            <h1>Contact information</h1>
            <ul>
                <li>24, Stuart Ave, Garden City, New York, 11530</li>
                <li>Email:<a href="mailto:support@familyHardware.com">support@familyHardware.com</a></li>
                <li>Phone: 555-555-5555</li>
            </ul>
        
    </div>
</asp:Content>
