﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Import statements for our database code
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Configuration;
// END OF DATABASE IMPORT STATEMENTES

namespace OnlineStore2.DBClasses
{ //Our product tier or data tier for use with our database.
    //A.K.A the data tier
    public class ProductTier
    {
        private string connectionString; //this is our connection string for the database

        public ProductTier()
        {
            /*our connectionstring will be set to the connection string 
              in our web.config file*/
            connectionString = ConfigurationManager.ConnectionStrings["MyData"].ToString();
        }


        //getter method to retrieve our product tier dataset.
        public DataSet getProductTierDataSet()
        {
            SqlConnection conn = new SqlConnection(connectionString); //open a new sql connection with our connection string passed as a parameter
            DataSet ds = new DataSet(); //what we are filling.
            string query = "SELECT * FROM Products;"; //Our sql query
            SqlDataAdapter adapter = new SqlDataAdapter(query,connectionString); //something is wrong here

            try //This is where we will fill our dataset
            {
                adapter.Fill(ds, "Products"); //the dataset will populate with items from our products table.

            }
            catch(SqlException e)
            {
                throw new Exception("Something went wrong in SQL: ", e);
            }
            finally //if everything works out in the end.
            {
                conn.Close();//closes connection after running the sql query.
                adapter.Dispose();//This will free up memory and dispose of anyhing done in the adapter.
                    //NOTE: this is only reccomended to be done with the data adapter.
                    
            };
            return ds; //we finally return our dataset.

        }


        //method for getting product image
        public Byte[] getProductImage(int productID) //To be called in our imageHandler file under ProcessRequest method.
        {
            Byte[] productImg = null;
            SqlConnection connection = new SqlConnection(connectionString); //initialize a connection to the database
            String query = "SELECT ProductImage FROM Products WHERE ProductID=@ID;"; //sql query to retrieve image from table.
            SqlCommand command = new SqlCommand(query, connection); //initialize a new sql command passing the query and the connection.
            command.Parameters.Add("@ID", SqlDbType.Int).Value = productID;//pass the product id into the parameter

            try
            {
                connection.Open();

                //execute the command
                var data = command.ExecuteScalar();//this method will return the data within the first field of the first record returned.
                //we now evaluate if the data is null i.e no image.
                if (data == DBNull.Value)
                {
                    productImg = null;
                }
                else
                {//if we have an image we typecase data to a byte array and store in productImg
                    productImg = (Byte[])data;
                }
            }

            catch (SqlException e)
            {
                throw new Exception("Something went wrong while getting the image: ", e);
            }

            finally
            {
                connection.Close();
            }


            return productImg; //return a byte array of the image
        }


        #region for inserting deleting and updating products in our products table.



        //Method to Insert a product into the table.
        public bool insertProduct(string productDescription, string productName, float productPrice, int quantityOnHand, int departmentID, int categoryID, byte[] productImage = null)
        {
            bool success = false; //this indicates whether the sql command was a success or not.
            string query = "INSERT INTO Products (ProductDescription,ProductName,ProductPrice,QuantityOnHand,DepartmentID,CategoryID,ProductImage) " + "VALUES(@description,@name,@price,@quantity,@departmentID,@categoryID,@image);"; //Sql command to insert with parameters

            SqlConnection connection = new SqlConnection(connectionString);//create a new connection object to the dbms passing in the connection string.
            SqlCommand command = new SqlCommand(query,connection); // this holds our sql connection and our query.
            // You use the SqlCommand object if you are working with sored procedures 

            //Now we are going to store our paramters from our method into the sql command
            command.Parameters.Add("@description", SqlDbType.VarChar, 300).Value= productDescription;
            command.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = productName;
            command.Parameters.Add("@price", SqlDbType.Float).Value = productPrice;
            command.Parameters.Add("@quantity", SqlDbType.Int).Value = quantityOnHand;
            command.Parameters.Add("@departmentID", SqlDbType.Int).Value = departmentID;
            command.Parameters.Add("categoryID", SqlDbType.Int).Value = categoryID;
            
           
            if (productImage == null)
            {
                command.Parameters.Add("@image", SqlDbType.Image).Value = DBNull.Value; //Dbnull is a null value for use in sql databases. DBNull != null.
            }
            else
            {
                command.Parameters.Add("@image", SqlDbType.Image).Value = productImage;
            }

            int rows = 0; //rows is set to a default value.
            try
            {
                //open a connection to dbms
                connection.Open();

                //return the number of rows affected by the query.
                rows = command.ExecuteNonQuery();

                //check if more than 0 rows have been affected.
                if(rows > 0)
                {
                    success = true; //will be true if rows affected is greater than 0.
                }

            }
            catch(SqlException e)
            {
                throw new Exception("Something went wrong while inserting ", e);
            }
            finally
            {
                connection.Close();
            }
            
            Console.WriteLine("Number of rows affected after query has been executed: " + rows);
            return success; 
        }

        //Method to update a product in the table given a product ID
        public bool updateProduct(int productID, string productDescription, string productName, float productPrice, int quantityOnHand, int departmentID, int categoryID, byte[] productImage = null)
        { //Implementation is very similar to Insert
            bool success = false; //this indicates whether the sql command was a success or not.
            string query = "UPDATE Products SET ProductName = @name, ProductDescription= @description, ProductPrice = @price, QuantityOnHand= @quantity, DepartmentID = @dept, CategoryID = @cat WHERE ProductID = @productID;"; //query command for update

            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(query, connection);

            //Command params
            command.Parameters.Add("@productID", SqlDbType.Int).Value = productID;
            command.Parameters.Add("@description", SqlDbType.VarChar, 300).Value = productDescription;
            command.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = productName;
            command.Parameters.Add("@price", SqlDbType.Float).Value = productPrice;
            command.Parameters.Add("@quantity", SqlDbType.Int).Value = quantityOnHand;
            command.Parameters.Add("@dept", SqlDbType.Int).Value = departmentID;
            command.Parameters.Add("@cat", SqlDbType.Int).Value = categoryID;

            int rows = 0; //rows that should be afftected

            try
            {
                connection.Open();
                rows = command.ExecuteNonQuery();
                if(rows > 0)
                {
                    success = true;
                }

            }

            catch(SqlException e)
            {
                throw new Exception("Something went wrong while updating the database. ",e);
            }
            finally
            {
                connection.Close();
            }

            return success;
        }
        
        //Method to delete a product in the table given a product ID
        public bool deleteProduct(int productID, string productDescription, string productName, float productPrice, int quantityOnHand, int departmentID, int categoryID, byte[] productImage = null) 
        { // implementation is very similar to insert
            bool success = false; //this indicates whether the sql command was a success or not.

            SqlConnection connection = new SqlConnection(connectionString);
            string query = "DELETE FROM Products WHERE ProductID=@ID AND ProductDescription=@description " + "AND ProductName=@name " + "AND ProductPrice=@price " + "AND QuantityOnHand=@quantity " + "AND DepartmentID=@dept AND CategoryID=@cat;";

            //Create new sql command object
            SqlCommand command = new SqlCommand(query, connection);
            
            //add parameters
            command.Parameters.Add("@ID", SqlDbType.Int).Value = productID;
            command.Parameters.Add("@description", SqlDbType.VarChar, 300).Value = productDescription;
            command.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = productName;
            command.Parameters.Add("@price", SqlDbType.Float).Value = productPrice;
            command.Parameters.Add("@quantity", SqlDbType.Int).Value = quantityOnHand;
            command.Parameters.Add("@dept", SqlDbType.Int).Value = departmentID;
            command.Parameters.Add("@cat", SqlDbType.Int).Value = categoryID;

            //command.Parameters.Add("@ID",db)

            int rows = 0;
            try
            {
                connection.Open();
                rows = command.ExecuteNonQuery();
                if(rows > 0)
                {
                    success = true;
                }
            }
            catch(SqlException e)
            {
                throw new Exception("Something went wrong while deleting: ", e);
            }

            finally
            {
                connection.Close();
            }

            return success;
        }
        #endregion
    }
}