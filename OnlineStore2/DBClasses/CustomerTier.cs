﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Import statements for our database code
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Configuration;
// END OF DATABASE IMPORT STATEMENTES

namespace OnlineStore2.DBClasses
{ //customer tier class for use with the databse.
    
    public class CustomerTier
    {
        private string connectionString; //field for connection string

        public CustomerTier() //Constructor
        {
            connectionString = ConfigurationManager.ConnectionStrings["MyData"].ToString();
        }


        //method to retrieve customer dataset.
        public DataSet getCustomerTierDataSet()
        {
            DataSet customer_ds = new DataSet();
            string query = "SELECT * FROM CustomerInformation ;";//sql commmand for getting data from CustomerInformation table
            SqlConnection connection = new SqlConnection(connectionString);

            SqlDataAdapter adapter = new SqlDataAdapter(query, connectionString);


            try
            {
                connection.Open();
                adapter.Fill(customer_ds, "CustomerInformation");
            }

            catch(SqlException e)
            {
                throw new Exception("Something went wrong in SQL: ", e);
            }

            finally
            {
                //connection
                connection.Close();
                adapter.Dispose();
            };


            return customer_ds;
        }

        #region update, insert, and delete snippets below

        //method to insert
        public bool insertCustomer(string fName, string lName, string addr1, string addr2, string city, string state, int zip, string mName = null)
        {
            bool success = false;
            SqlConnection connection = new SqlConnection(connectionString);
            string query = "INSERT INTO CustomerInformation (FirstName,MiddleName,LastName,Address,Address2,City,State,Zip) " + "VALUES (@fName,@mName,@lName,@addr1,@addr2,@city,@state,@zip);";

            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.Add("@fName", SqlDbType.VarChar, 50).Value = fName;
            command.Parameters.Add("@mName", SqlDbType.VarChar, 50).Value = DBNull.Value;
            command.Parameters.Add("@lName", SqlDbType.VarChar, 50).Value = lName;
            command.Parameters.Add("@addr1", SqlDbType.VarChar, 50).Value = addr1;
            command.Parameters.Add("@addr2", SqlDbType.VarChar, 50).Value = addr2;
            command.Parameters.Add("@city", SqlDbType.VarChar, 50).Value = city;
            command.Parameters.Add("@state", SqlDbType.VarChar, 50).Value = state;
            command.Parameters.Add("@zip", SqlDbType.Int).Value = zip;

            int rows = 0;

            try
            {
                connection.Open();
                rows = command.ExecuteNonQuery();
                
                if(rows > 0)
                {
                    success = true;
                }                
            }
            catch(SqlException e)
            {
                throw new Exception("Something went wrong while inserting ", e);
            }

            finally
            {
                connection.Close();
            }

            Console.WriteLine("Number of rows affected after query has been executed: " + rows);

            return success;
        }


        //method to update
        public bool updateCustomer(int custID, string fName, string lName, string addr1, string addr2, string city, string state, int zip, string mName=null)
        {
            bool success = false;
            string query = "UPDATE CustomerInformation SET FirstName = @fName, MiddleName=@mName, LastName = @lName , Address = @addr1, Address2= @addr2, City = @city, State = @state, Zip = @zip WHERE CustID = @custID;"; //query command for update

            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(query, connection);

            command.Parameters.Add("@custID", SqlDbType.Int).Value = custID;
            command.Parameters.Add("@fName", SqlDbType.VarChar, 50).Value = fName;
            command.Parameters.Add("@mName", SqlDbType.VarChar, 50).Value = DBNull.Value;
            command.Parameters.Add("@lName", SqlDbType.VarChar, 50).Value = lName;
            command.Parameters.Add("@addr1", SqlDbType.VarChar, 50).Value = addr1;
            command.Parameters.Add("@addr2", SqlDbType.VarChar, 50).Value = addr2;
            command.Parameters.Add("@city", SqlDbType.VarChar, 50).Value = city;
            command.Parameters.Add("@state", SqlDbType.VarChar, 50).Value = state;
            command.Parameters.Add("@zip", SqlDbType.Int).Value = zip;

            int rows = 0;

            try
            {
                connection.Open();
                rows = command.ExecuteNonQuery();
                if (rows > 0)
                {
                    success = true;
                }
            }

            catch (SqlException e)
            {
                throw new Exception("Something went wrong while updating ",e);
            }

            finally
            {
                connection.Close();
            }


            return success;
        }


        //method to delete
        public bool deleteCustomer(int custID)
        {
            bool success = false;
            string query = "DELETE FROM CustomerInformation WHERE CustID = @custID;";
            SqlConnection connection = new SqlConnection(connectionString);

            SqlCommand command = new SqlCommand(query, connection);

            command.Parameters.Add("@custID", SqlDbType.Int).Value = custID;

            int rows = 0;

            try
            {
                connection.Open();
                rows = command.ExecuteNonQuery();
                if (rows > 0)
                {
                    success = true;
                }
            }

            catch (SqlException e)
            {
                throw new Exception("Something went wrong while updating ", e);
            }

            finally
            {
                connection.Close();
            }


            return success;
        }


        #endregion


    }
}