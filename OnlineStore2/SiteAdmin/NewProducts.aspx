﻿<%@ Page Title="" Language="C#" MasterPageFile="AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="NewProducts.aspx.cs" Inherits="OnlineStore2.SiteAdmin.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <!-- code for the body-->
    <div class="container">
        <h1 class="jumbotron">Product Information</h1>
        <!-- add a gridview to display all the available products in the database-->
        <asp:GridView  ID="gvProducts" AllowPaging="true" AutoGenerateColumns="false" DataKeyNames="ProductID" runat="server" OnPageIndexChanging="changePage" OnRowDeleting="gv_rowDelete" OnRowEditing="gv_rowUpdate" Width="100%">
           
            <Columns>
                <asp:CommandField HeaderText="Delete" ShowDeleteButton="true" />
                <asp:CommandField HeaderText="Update" ShowEditButton="true" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="ProductID" HeaderText="ID" ReadOnly="false" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="ProductName" HeaderText="Product Name" ReadOnly="false" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="ProductDescription" HeaderText="Description" ReadOnly="false" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="ProductPrice" HeaderText="Price" ReadOnly="false" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="QuantityOnHand" HeaderText="Quantity" ReadOnly="false" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="DepartmentID" HeaderText="Department ID" ReadOnly="false" />
                <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="CategoryID" HeaderText="Category ID" ReadOnly="false" />
                <asp:ImageField DataImageUrlField="ProductID" DataImageUrlFormatString="../imgHandler.ashx?imgID={0}" ControlStyle-Height="50" ControlStyle-Width="50"></asp:ImageField>    
            </Columns>
        </asp:GridView>
        
        <!--code below is very similar to the customer registration form.-->
        <form runat="server" id="form1">
            <div class="panel panel-success">
                <div class="panel panel-heading text-center">
                
                    <h3>Enter New Product Information</h3>
                </div>
                <div class="panel-body">
                    <p id="success" style="color:green;" runat="server"></p>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="BulletList" ShowSummary="true" ForeColor="Red" HeaderText="Please fix the following" />
                    <!--form controls are displayed below-->
                    <table>
                        <tr>
                            <td>
                                 <!--Input for productname -->
                                <label>
                                    Product Name *
                                </label>
                                <asp:TextBox ID="txtProductName" placeHolder="Product Name" runat="server" CssClass="form-control" ></asp:TextBox>
                            </td>
                            <td>
                                <!-- regex validator for product name -->
                                <asp:RegularExpressionValidator ID="regexProductName" runat="server" ErrorMessage="Must be no less than 50 characters long" ValidationExpression="[a-z-A-Z0-9_ ]{1,50}" ControlToValidate="txtProductName" ForeColor="Red"></asp:RegularExpressionValidator><br/>
                                <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="product name is missing" ControlToValidate="txtProductName" ForeColor="Red"></asp:RequiredFieldValidator>

                            </td>
                        </tr>
                        <tr>
                                <!--input for product description--> 
                                <td>
                                    <label>
                                        Product Description *
                                    </label>
                                    <asp:TextBox ID="txtProductDescription" placeHolder="Product Description" CssClass="form-control" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <!--regex for product description -->
                                    <asp:RegularExpressionValidator ID="regexProductDescription" runat="server" ErrorMessage="Must be no longer than 300 characters long." ValidationExpression="[a-zA-Z_ ]{3,300}" ControlToValidate="txtProductDescription" ForeColor="Red"></asp:RegularExpressionValidator><br />
                                    <asp:RequiredFieldValidator ID="rfvdescription" runat="server" ErrorMessage="product description is missing" ControlToValidate="txtProductDescription" ForeColor="Red"></asp:RequiredFieldValidator>
                                </td>
                         </tr>
                        <tr>
                            <!--input for product price-->
                            <td>
                                <label>
                                    Product Price *
                                </label>
                                <asp:TextBox ID="txtPrice" placeHolder="Product Price" runat="server" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td>
                                <!--regex validator for product price -->
                                <asp:RegularExpressionValidator ID="regexPrice" runat="server" ErrorMessage="Must be a float and no letters" ValidationExpression="[-+]?[0-9]*\.?[0-9]+." ControlToValidate="txtPrice" ForeColor="Red"></asp:RegularExpressionValidator><br />
                                <asp:RequiredFieldValidator ID="rfvprice" runat="server" ErrorMessage="product price is missing" ControlToValidate="txtPrice" ForeColor="Red"></asp:RequiredFieldValidator>

                            </td>
                        </tr>
                        <tr>
                            <!--input for quantity-->
                            <td>
                                <label>
                                    Quantity on Hand *
                                </label>
                                <asp:TextBox ID="txtQuantity" runat="server" placeHolder="Quantity on Hand" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td>
                                <!--regex for quantity-->
                            <asp:RegularExpressionValidator ID="regexQuantity" runat="server" ErrorMessage="No letters. Numbers only" ControlToValidate="txtQuantity" ValidationExpression="^[0-9]{1,45}$" ForeColor="Red"></asp:RegularExpressionValidator><br />
                                <asp:RequiredFieldValidator ID="rfvquantity" runat="server" ErrorMessage="Quantity is missing" ControlToValidate="txtQuantity" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                        
                        </tr>
                        <tr>
                            <td>
                                <!--Input for department ID -->
                                <label>
                                    Department ID *
                                </label>
                                <asp:TextBox ID="txtDepartment" runat="server" placeHolder="Department ID" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td>
                                <!--regex for department ID -->
                                <asp:RegularExpressionValidator runat="server" ID="regexDepartment" ErrorMessage="Numbers Only. No letters" ControlToValidate="txtDepartment" ValidationExpression="^[0-9]{1,45}$" ForeColor="Red"></asp:RegularExpressionValidator><br />
                                <asp:RequiredFieldValidator ID="rfvdept" runat="server" ErrorMessage="Department ID is missing" ControlToValidate="txtDepartment" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <!--input for category id -->
                            <!--make a dropdown menu for categories instead of listing category ID. that way you can get the cat id by referencing the category type -->
                            <td>
                                <label>
                                    Category ID *
                                </label>
                                <asp:TextBox ID="txtCategory" runat="server" placeHolder="Category ID" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td>
                                <!--regex for category id-->
                                 <asp:RegularExpressionValidator runat="server" ID="regexCategory" ErrorMessage="Numbers Only. No letters" ControlToValidate="txtCategory" ValidationExpression="^[0-9]{1,45}$" ForeColor="Red"></asp:RegularExpressionValidator><br />
                                <asp:RequiredFieldValidator ID="rfvcat" runat="server" ErrorMessage="Category ID is missing" ControlToValidate="txtCategory" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    Product Image (optional)
                                </label>
                                <!--add an file upload control here-->
                                <asp:FileUpload ID="FileUpload1" runat="server" CssClass="form-control" />
                            </td>
                        
                        </tr>                  
                    </table>
                    <div>
                        <asp:Button ID="uploadImg" CausesValidation="false" Text="Add Product" runat="server" CssClass="form-control" onClick="submit_data"/>
                    </div>
                </div>
            </div>
        </form>

    </div>
</asp:Content>
