﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAdmin/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="CustomerAdmin.aspx.cs" Inherits="OnlineStore2.SiteAdmin.CustomerAdmin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
        <div class="container">
            <h1 class="jumbotron">Customer Management</h1>
            <asp:GridView ID="gvCustomers" AllowPaging="false" DataKeyNames="CustID" runat="server" OnPageIndexChanging="" OnRowDeleting="gvCustomersDeleting" OnRowEditing="gvCustomersEditing" Width="100%">
                <Columns>
                    <asp:CommandField ShowDeleteButton="true" />
                    <asp:CommandField ShowEditButton="true" />
                    <asp:BoundField DataField="CustID" HeaderText="Customer ID" ItemStyle-HorizontalAlign="Center" ReadOnly="false" />

                </Columns>
            </asp:GridView>
        </div>
</asp:Content>
