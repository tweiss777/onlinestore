﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnlineStore2.DBClasses;
using System.Data;
using System.IO;


namespace OnlineStore2.SiteAdmin
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        //helper method to convert a stream to a byte array
        private Byte [] convert_image(Stream fstream)
        {
            //Pass this as arg to fstream  when calling: FileUpload1.PostedFile.InputStream;

            BinaryReader br = new BinaryReader(fstream);//new binary reader holding the fstream.


            Byte[] img = br.ReadBytes((Int32)fstream.Length); //write the image to a byte array. typecast to 32 bit in
            return img;//return the byte array to be passed

        }
        #region table update and delete methods
        //method to update row in a table
        protected void gv_rowUpdate(object sender, GridViewEditEventArgs e)
        {
           
        }

       protected void gv_rowDelete(object sender, GridViewDeleteEventArgs e)//button works
        {
            //Response.Redirect("admin404Redirect.aspx");
            TableRow row = gvProducts.Rows[e.RowIndex];
            // row.Cells[i].Text is what you need to pass in you need indexes 1-7
            int productID = int.Parse(row.Cells[2].Text); //holds product id
            string name = row.Cells[3].Text; // holds product description
            string description = row.Cells[4].Text;//holds product name
            float price = float.Parse(row.Cells[5].Text); // holds product price 
            int quantity = int.Parse(row.Cells[6].Text); // holds the quantity on hand 
            int department_id = int.Parse(row.Cells[7].Text); // holds department id
            int category_id = int.Parse(row.Cells[8].Text); // holds category id

            //instantiate product tier object
            ProductTier pt = new ProductTier();

            //delete entry
            bool was_success = pt.deleteProduct(productID, description, name, price, quantity, department_id, category_id);

            if (was_success == true)
            {
                success.InnerText = "the row with a product id of " + productID.ToString() + " was successfully deleted!";
            }            

            //update table and bind data
            DataSet updated_ds = new DataSet();
            try
            {
                updated_ds = pt.getProductTierDataSet();
            }
            catch(Exception ex)
            {
                Response.Redirect("admin404Redirect.aspx");
            }
            //set the datasource of the grid to the dataview.
            gvProducts.DataSource = updated_ds;

            //Set the data member of the gridview to the name of the table in the database.
            gvProducts.DataMember = "Products";

            //bind the dataset to the gridview
            gvProducts.DataBind();
        }
        #endregion
        protected void changePage(object sender, GridViewPageEventArgs e)
        { //changes the page within a grid view
            gvProducts.PageIndex = e.NewPageIndex;

        }


        protected void submit_data(object sender, EventArgs e)
        {//Method to submit data to datatier to store in db.
            //will handle the image here by calling our helper method.
            string product_name = txtProductName.Text;
            string product_description = txtProductDescription.Text;

            float product_price;
            int quantity, deptID, catID;

            try
            { //validate typecasting
                 product_price = float.Parse(txtPrice.Text);
                 quantity = int.Parse(txtQuantity.Text);
                 deptID = int.Parse(txtDepartment.Text); //department id
                 catID = int.Parse(txtCategory.Text); //category id
            }
            catch(FormatException ex)
            {//something goes wrong..
                Response.Redirect("Admin404Redirect.aspx");
                throw new Exception("Something went wrong while typecasting. ", ex);
            }
            byte[] img = convert_image(FileUpload1.PostedFile.InputStream); //will return the image in the form of a byte array.

            //Initialize a new ProductTier object.
            ProductTier pt = new ProductTier();
            try
            {
                pt.insertProduct(product_description, product_name, product_price, quantity, deptID, catID, img);
            }
            catch(Exception ex)
            {
                Response.Redirect("Admin404Redirect.aspx");
                throw new Exception("Something went wrong while submitting to the datatier ", ex);
            }

            success.InnerHtml = "The information has been sent to the database";
            
            //set these text fields empty
            //do this upon successful submission to the database
            txtProductName.Text = "";
            txtProductDescription.Text = "";
            txtQuantity.Text = "";
            txtPrice.Text = "";
            txtDepartment.Text = "";
            txtCategory.Text = "";



        }
        
        /*This method basically shuts up the error that your 
         GridView Tags need runat="server"*/
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            

            //the DataSet is where the data from the table gets loaded onto a grid view
            DataSet dataset = new DataSet();
            //How the database is accessed.
            ProductTier product_tier = new ProductTier();
            try
            {
                dataset = product_tier.getProductTierDataSet(); //gets the current dataSet from the database.
            }
            catch(Exception ex)
            {//If something goes bad while trying to get the dataset from the product tier.
                //redirect the user to a 404 error page
                Response.Redirect("admin404Redirect.aspx");
                throw new Exception("Something went wrong here. ", ex);

            }
            //The following lines of code can be used assuming you are not using an objectdatasourceid
            //After the try catch, we take our dataset and store it to our grid.
     
            //set the datasource of the grid to the dataview.
            gvProducts.DataSource = dataset;

            //Set the data member of the gridview to the name of the table in the database.
            gvProducts.DataMember = "Products";

            //bind the dataset to the gridview
            gvProducts.DataBind();
            
        }

        
    }
}