﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnlineStore2.DBClasses;
using System.Data;
using System.IO;


namespace OnlineStore2.SiteAdmin
{
    public partial class CustomerAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //the DataSet is where the data from the table gets loaded onto a grid view
            DataSet dataset = new DataSet();
            //How the database is accessed.
            CustomerTier ct = new CustomerTier();
            try
            {
                dataset = ct.getCustomerTierDataSet(); //gets the current dataSet from the database.
            }
            catch (Exception ex)
            {//If something goes bad while trying to get the dataset from the product tier.
                //redirect the user to a 404 error page
                Response.Redirect("admin404Redirect.aspx");
                throw new Exception("Something went wrong here. ", ex);

            }
            //The following lines of code can be used assuming you are not using an objectdatasourceid
            //After the try catch, we take our dataset and store it to our grid.

            //set the datasource of the grid to the dataview.
            gvCustomers.DataSource = dataset;

            //Set the data member of the gridview to the name of the table in the database.
            gvCustomers.DataMember = "CustomerInformation";

            //bind the dataset to the gridview
            gvCustomers.DataBind();
        }


        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        protected void gvCustomers_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Response.Redirect("admin404Redirect.aspx");
        }

        protected void gvCustomers_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void gvCustomers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }
    }
}
