﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAdmin/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="AdminDefault.aspx.cs" Inherits="OnlineStore2.SiteAdmin.AdminDefault" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="jumbotron">
        <h1>Select option from menu above.</h1>
    </div>
    <div class="container pull-left">
        <div class="row">
            <div class="col-lg-5 col-sm-5">
                <a href="CustAdmin2.aspx" style="font-size:large">Customers</a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5 col-sm-5">
                <a href="NewProducts.aspx" style="font-size:large">Products</a>
            </div>
        </div>
    </div>
</asp:Content>
