﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAdmin/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="CustAdmin.aspx.cs" Inherits="OnlineStore2.SiteAdmin.CustAdmin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <!--Use a table as opposed to a gridview-->
    <div class="container">
        <asp:GridView EnableViewState="false" ID="gvCustomer" AutoGenerateColumns="false" DataKeyNames="CustID" runat="server" OnRowDeleting="delete" OnRowEditing="edit" OnPageIndexChanging="change_page" AllowPaging="true" Width="100%">
        <Columns>
            <asp:CommandField ShowDeleteButton="true" />
            <asp:CommandField ShowEditButton="true" />
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="CustID" HeaderText="ID" ReadOnly="false" />
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="FirstName" HeaderText ="First Name" ReadOnly="false" />
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="LastName" HeaderText ="Last Name" ReadOnly="false" />
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Address" HeaderText="Address 1" ReadOnly="false" />
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Address2" HeaderText="Address 2" ReadOnly="false" />
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="City" HeaderText="City" ReadOnly="false" />
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="State" HeaderText="State" ReadOnly="false" />
            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="Zip" HeaderText="Zip" ReadOnly="false" />
        </Columns>
    </asp:GridView>
    </div>
    
</asp:Content>
