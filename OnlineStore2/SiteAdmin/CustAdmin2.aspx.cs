﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using OnlineStore2.DBClasses;

namespace OnlineStore2.SiteAdmin
{
    public partial class CustAdmin2 : System.Web.UI.Page
    {
        //Look into adding a button programatically to the table
        protected void Page_Load(object sender, EventArgs e)
        {
            #region snippet to build table headers
            string[] headers = {"Delete","Edit", "ID", "First Name","Middle Name","Last Name", "Address 1", "Address 2", "City", "State", "Zip" }; //array of table headers
            Table dt = new Table();
            TableRow tbl_headers = buildTableHeadings(headers);
            dt.Attributes.Add("border", "1");
            dt.Rows.Add(tbl_headers);
            #endregion


            #region snippet to add buttons and datafields
            TableRow tr; //for the table row data
            TableCell td; //for the cell data
            Label lbl;
            Button delbtn;
            Button editbtn;
            CustomerTier ct = new CustomerTier();
            DataSet ds = ct.getCustomerTierDataSet();
            foreach(DataRow row in ds.Tables[0].Rows) {
                tr = new TableRow();
                
                //code snippet for delete button
                td = new TableCell();
                delbtn = new Button();
                delbtn.Text = "Delete";
                delbtn.ID = row.ItemArray[0].ToString()+'d';
                delbtn.CommandArgument = row.ItemArray[0].ToString(); //passes customer id to delete command
                delbtn.Command += delete; //assign the event handler
                //assign event handler here

                td.Controls.Add(delbtn);
                tr.Cells.Add(td);

                //code snippet for edit b
                td = new TableCell();
                editbtn = new Button();
                editbtn.Text = "Edit";
                editbtn.ID = row.ItemArray[0].ToString()+'e';
                editbtn.CommandArgument = row.ItemArray[0].ToString(); //passes customer id to edit command
                editbtn.Command += edit; //assign the event handler
                             
                
                //assign event handler here
                td.Controls.Add(editbtn);
                tr.Cells.Add(td);

                foreach (var col in row.ItemArray)
                {
                    //initialize new tablecell and label objects
                    td = new TableCell();
                    lbl = new Label();
                
                    lbl.Text = col.ToString();//give label data
                    td.Controls.Add(lbl);//bind label to column 
                    tr.Cells.Add(td);//append the table cells to your table row

                }

                dt.Rows.Add(tr);
                #endregion
            }
            customerpnl.Controls.Add(dt);


        }


        private TableRow buildTableHeadings(String [] tblHeadings)
        {
            TableRow tr = new TableRow(); //declare a new Tablerow object (this is your <tr>)
            TableCell td;//declare a new TableCell object (this is your <td>)
            Label cellLbl ;//declare a new label to go into the cell(Apply text to the table data)

            foreach(string header in tblHeadings)
            {
                //initialize new objects
                td = new TableCell();
                cellLbl = new Label();

                cellLbl.Text = header;//give table cell text

                td.Controls.Add(cellLbl); //add the label to the td cell
                tr.Cells.Add(td); //append td to you tablerow
                
               
                
            }

            return tr;
        }

        #region edit and delete from table snippet
        public void delete(object sender,EventArgs e)
        {
            CustomerTier ct = new CustomerTier();
            Button btnData = (Button)sender;
            int custID = int.Parse(btnData.CommandArgument);
            bool was_success = ct.deleteCustomer(custID);

            if (was_success == true)
            {
                success.InnerText = "The Customer with a Customer ID of " + custID.ToString() + " was successfully deleted.";
            }
            else
            {
                Response.Redirect("admin404Redirect.aspx");
            }
            
        }

        public void edit(object sender, EventArgs e)
        {

        }
        #endregion
    }
}