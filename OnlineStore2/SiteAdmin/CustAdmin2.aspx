﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteAdmin/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="CustAdmin2.aspx.cs" Inherits="OnlineStore2.SiteAdmin.CustAdmin2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="container">
        <h1 class="jumbotron">
            Customer Data
        </h1>
        <form runat="server" id="form1">
            <p id="success" style="color:green" runat="server"></p>
            <asp:panel runat="server" id="customerpnl">

            </asp:panel>
        </form>
    </div>
</asp:Content>
