﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnlineStore2.DBClasses;
using System.Data;
using System.IO;

namespace OnlineStore2.SiteAdmin
{
    public partial class CustAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DataSet ds = new DataSet();

                CustomerTier ct = new CustomerTier();

                try
                {
                    ds = ct.getCustomerTierDataSet();
                }
                catch (Exception ex)
                {
                    Response.Redirect("admin404Redirect.aspx");
                    throw new Exception("Something went wrong ", ex);
                }

                gvCustomer.DataSource = ds;

                gvCustomer.DataMember = "CustomerInformation";

                gvCustomer.DataBind();
            }
            
            
        }

        public override void VerifyRenderingInServerForm(Control control)
        {

        }

       protected void change_page(object sender, GridViewPageEventArgs e)
        {

        }

        protected void delete(object sender, GridViewDeleteEventArgs e)
        {
            Response.Redirect("admin404Redirect.aspx");
        }

        protected void edit(object sender, GridViewEditEventArgs e)
        {
             
        }

    }
}