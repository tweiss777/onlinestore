<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="OnlineStore2.About" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="container">
        <div class="panel panel-default container-fluid">
            <div class="panel-heading">
                <h2>About Us</h2>
            </div>
            <div class="panel-body" style="font-size:large">
                <i>"Ride with me through the journey to more success" - DJ Khaled</i><br /><br />
                     <div class="imageContainer pull-right">
                         <img class="img-responsive pull-right" src="https://cdn.modernfarmer.com/wp-content/uploads/2013/04/hardware-1.jpg" />
                              One of our imaginary hardware stores 
                              located all the way up in Montauk, NY ;)<br />
                     </div>
                     In 1924, to increase buying power and profits, our entrepreneurs  united their 
                     Chicago, Illinois hardware stores into "PowerTools Inc." and have relocated the company to NY. 
                     As of today we are the largest manufacturer and distributor of hardware and supplies
                     in all of Long Island.<br />
                We have been selling a vast array of tools and supplies to the people of Long Isalnd for as long as we can remember. We have been awarded with the 
                Accredited business award and have recieved awards from J.D. power association, and many other organizations that you could possibly imagine.
                <br />
            </div>

                 
            

            <div class="panel-body">
                <h2>Some of our tools and hardware</h2>
                <!--Consider making a grid for this section-->
                <!--Left column would have a description. Right column would have image-->
                <div class="container"> <!--container encapsulating our grid-->
                    <div class="row">
                        <div class="col-md-5 col-sm-5">
                            <h3>HackSaw</h3>
                            <p> fine-toothed saw, originally and principally made for cutting metal.

Most hacksaws are hand saws with a C-shaped frame that holds a blade under tension. Such hacksaws have a handle, usually a pistol grip, with pins for attaching a narrow disposable blade. The frames may also be adjustable to accommodate blades of 
different sizes. A screw or other mechanism is used to put the thin blade under tension.

On hacksaws, as with most frame saws, the blade can be mounted with the teeth facing toward or away from the handle, resulting in cutting action on either the push or pull stroke. In normal use, cutting vertically downwards with work held in a bench 
vice, hacksaw blades are set to be facing forwards.
</p>
                            <i>Source: Wikipedia</i>  
                         </div>
                        <div class="col-md-5 col-sm-5">
                            <div class="pull-right">
                                 <img class="img-responsive" src="https://upload.wikimedia.org/wikipedia/commons/9/9b/Hacksaw.jpg" />
                            </div>
                           
                        </div>
                </div>
                <div class="row">
                    <div class="col-md-5 col-sm-5">
                        <h3>Hammer</h3>
                        <p>
                             tool or device that delivers a blow (a sudden impact) to an object. Most hammers are hand tools used to drive nails, fit parts, forge metal, and break apart objects. Hammers vary in shape, size, and structure, depending on their purposes.

Hammers are basic tools in many trades. The usual features are a head (most often made of steel) and a handle (also called a helve or haft). Although most hammers are hand tools, powered versions exist; they are known as powered hammers. Types of power hammer include steam hammers and trip hammers, often for heavier uses, such as forging.

Some specialized hammers have other names, such as sledgehammer, mallet, and gavel. The term "hammer" also applies to devices that deliver blows, such as the hammer of a firearm, or the hammer of a piano, or the hammer ice scraper.
                        </p>
                        <i>Source: Wikipedia</i>
                    </div>
                    <div class="col-md-5 col-sm-5 ">
                        <div class="pull-right" >
                            <img class="img-responsive"src="https://upload.wikimedia.org/wikipedia/commons/8/84/Claw-hammer.jpg" />
                        </div>
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5 col-sm-5">
                        <h3>Wrenches</h3>
                        <p> provide grip and mechanical advantage in applying torque to turn objects�usually rotary fasteners, such as nuts and bolts�or keep them from turning.

In Commonwealth English (excluding Canada), spanner is the standard term. The most common shapes are called open-ended spanner and ring spanner. The term wrench is generally used for tools that turn non-fastening devices (e.g. tap wrench and pipe wrench), or may be used for a monkey wrench - an adjustable spanner.[1]

In North American English, wrench is the standard term. The most common shapes are called open-end wrench and box-end wrench. In American English, spanner refers to a specialised wrench with a series of pins or tabs around the circumference. (These pins or tabs fit into the holes or notches cut into the object to be turned.) In American commerce, such a wrench may be called a spanner wrench to distinguish it from the British sense of spanner.

Higher quality wrenches are typically made from chromium-vanadium alloy tool steels and are often drop-forged. They are frequently chrome-plated to resist corrosion and for ease of cleaning.</p>
                        <i>Source: Wikipedia</i>
                    </div>
                    <div class="col-md-5 col-sm-5">
                        <img class="img-responsive" src="https://upload.wikimedia.org/wikipedia/commons/1/1c/Gedore_No._7_combination_wrenches_6%E2%80%9319_mm.jpg" />
                    </div>
                </div>

                  
                </div>

            </div>

        </div>

    </div>

</asp:Content>

